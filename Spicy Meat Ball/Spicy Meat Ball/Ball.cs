﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;

namespace Spicy_Meat_Ball
{
    public class Ball : DynamicCircSprite
    {
        //settings
        private int speed;

        //sound fx stuff
        private SoundEffect hitSound, dashSound;
        public Boolean soundFx;

        public Ball(ContentManager contentmanager)
            : base()
        {
            this.speed = 4;

            //sound effects
            soundFx = true;
            hitSound = contentmanager.Load<SoundEffect>("hit");
            dashSound = contentmanager.Load<SoundEffect>("dash");
        }

        public override void SetBody(World world, Vector2 vPosition, float fMass = 1.0f, float fFriction = 0.0f, float fRestitution = 1.0f)
        {
            if (this.body != null) this.body.Dispose();

            base.SetBody(world, vPosition, fMass, fFriction, fRestitution);

            this.body.LinearDamping = 0.5f / this.Speed;
            this.body.FixedRotation = true;

            this.body.OnCollision += MyOnCollision;
        }

        public override bool MyOnCollision(Fixture f1, Fixture f2, Contact contact)
        {
            //play sound
            if (soundFx) hitSound.Play();

            //We want the collision to happen, so we return true.
            return true;
        }

        public int Speed
        {
            get { return this.speed; }
            set
            {
                this.speed = value;
                this.body.LinearDamping = 0.5f / this.speed;
            }
        }
        
    }
}
