﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Spicy_Meat_Ball
{
    public class Goal:Sprite
    {
        private int nummeatballs = 0;
        private List<Vector2> meatballpositions;
        private Texture2D meatballImage;

        public int NumberMeatballs
        {
            get { return nummeatballs; }
            set { nummeatballs = value; }
        }

        public Goal():base()
        {
                      
        }

        public override Vector2 Position
        {
            get
            {
                return base.Position;
            }
            set
            {
                base.Position = value;
                meatballpositions = null;
            }
        }
        public override void SetImage(ContentManager contentmanager, string Name, float scale = 1, float rotate = 0, SpriteEffects spriteEffect = SpriteEffects.None)
        {
            base.SetImage(contentmanager, Name, scale, rotate, spriteEffect);
            this.meatballImage = contentmanager.Load<Texture2D>("meatball");            
        }

        public override void Draw(SpriteBatch spritebatch)
        {
            base.Draw(spritebatch);

            if (this.meatballpositions == null)
            {
                float cX = this.Origin.X, cY = this.Origin.Y, rg = this.Width / 2, rm = this.meatballImage.Width / 2;
                this.meatballpositions = new List<Vector2>  { 
                     new Vector2(cX, cY-rg+rm), new Vector2(cX, cY + (rg/2) - rm), new Vector2(cX - (rg - rm), cY), 
                     new Vector2(cX + (rg/4), cY-rm), new Vector2(cX - (2 * rm), cY-(rm * 2))
                 };
            }

            for (int i = 0; i < this.NumberMeatballs; i++)
            {
                spritebatch.Draw(meatballImage, this.meatballpositions[i], Color.White);
            }
        }

    }
}
