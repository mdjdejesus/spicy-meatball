﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Spicy_Meat_Ball
{
    class MenuButton
    {
        Texture2D texture;
        Vector2 position;
        Rectangle rectangle;

        Color color = new Color(255, 255, 255, 255);

        public Vector2 size;

        public MenuButton(Texture2D newTexture, GraphicsDevice graphics)
        {
            texture = newTexture;

            size = new Vector2(graphics.Viewport.Width / 8, graphics.Viewport.Height / 30);

        }

        public Vector2 Position
        { get { return position; } }

        bool down;
        public bool isClicked;
        private bool isMouseDown; 

        public void Update(MouseState mouse)
        {
            rectangle = new Rectangle((int)position.X, (int)position.Y, (int)size.X, (int)size.Y);

            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);

            ButtonState bs = mouse.LeftButton;

            //handle the gamepad button
            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed)
                {
                    if (mouse.LeftButton != ButtonState.Pressed) bs = ButtonState.Pressed;
                }
                else
                {
                    if (mouse.LeftButton == ButtonState.Released && isMouseDown) bs = ButtonState.Released;
                }
             
            }

            if (mouseRectangle.Intersects(rectangle))
            {
                if (color.A == 255) down = false;
                if (color.A == 0) down = true;
                if (down) color.A += 3; else color.A -= 3;
                
                if (bs == ButtonState.Pressed)
                {
                    isMouseDown = true;
                }
                else
                {
                    if (isMouseDown)
                    {
                        isClicked = true;
                    }
                    else { isClicked = false; }

                    isMouseDown = false;
                }
            }
            else if (color.A < 255)
            {
                color.A += 3;
                isClicked = false;
                isMouseDown = false; 
            }
            else
            {
                isClicked = false; 
                isMouseDown = false; 
            }

        }

        public void setPosition(Vector2 newPosition)
        {
            position = newPosition;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, rectangle, color);
        }

    }
}
