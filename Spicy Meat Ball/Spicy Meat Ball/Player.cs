﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics;

namespace Spicy_Meat_Ball
{
    public class Player : DynamicCircSprite
    {
        //settings
        private int speed;

        //sound fx stuff
        private SoundEffect hitSound, dashSound, stopSound;
        public Boolean soundFx;

        //DASH stuff
        public Boolean IsCoolingDown;
        public Boolean IsDashing;
        private int CoolDownCounter, DashCounter;
        private int CoolDownCounterMax = 2000, DashCounterMax = 500;       
        private Sprite DashImage;

        public Player(ContentManager contentmanager)
            : base()
        {
            this.speed = 4; //default to fast

            this.DashImage = new Sprite(); //init sprite image

            //sound effects
            soundFx = true;
            hitSound = contentmanager.Load<SoundEffect>("hit");
            dashSound = contentmanager.Load<SoundEffect>("dash");
            stopSound = contentmanager.Load<SoundEffect>("stop");
        }

        public override void SetBody(World world, Vector2 vPosition, float fMass = 1.0f, float fFriction = 0.0f, float fRestitution = 1.0f)
        {
            base.SetBody(world, vPosition, fMass, fFriction, fRestitution);

            this.body.LinearDamping = 0.5f / this.Speed;
            this.body.FixedRotation = true;

            this.body.OnCollision += MyOnCollision;
        }

        public override void ApplyLinearImpulse(Vector2 vDir)
        {
            base.ApplyLinearImpulse(2 * this.Speed * vDir);
        }

        public override bool MyOnCollision(Fixture f1, Fixture f2, Contact contact)
        {
            //play sound
            if (soundFx) hitSound.Play();

            //We want the collision to happen, so we return true.
            return true;
        }

        public int Speed
        {
            get { return this.speed; }
            set
            {
                this.speed = value;
                this.body.LinearDamping = 0.5f / this.speed;
            }
        }

        public override void Stop()
        {
            //play sound
            if (soundFx && this.body.LinearVelocity.Length() > 0.5f) stopSound.Play();
            base.Stop();
           
        }

        public void SetPlayerImages(ContentManager contentmanager, string Name, string dashName, float scale = 1, float rotate = 0, SpriteEffects spriteEffect = SpriteEffects.None)
        {
            base.SetImage(contentmanager, Name, scale, rotate, spriteEffect);
            this.DashImage.SetImage(contentmanager, dashName, scale, rotate, spriteEffect);
        }

        public override void Draw(SpriteBatch spritebatch)
        {
            //draw dash stuff
            if (IsDashing && this.body.LinearVelocity.Length() > 0.1f)
            {
                Vector2 pos = ConvertUnits.ToDisplayUnits(this.body.Position);
                Vector2 dir = -this.body.LinearVelocity;
                float rot = (float)Math.Atan2(dir.Y, dir.X);
                spritebatch.Draw(this.DashImage.Image, pos, null, Color.White,
                    rot, new Vector2(0, this.DashImage.Height / 2), 1.0f, SpriteEffects.None, 0f);
            }

            //draw the player
            base.Draw(spritebatch);
        }

       // ********************** DASH STUFF BELOW ***************************

        public Boolean StartDash()
        {
            if (this.IsCoolingDown == false)
            {
                //Dash
                Vector2 vdir = this.body.LinearVelocity;
                if (this.body.LinearVelocity != Vector2.Zero)
                {
                    vdir.Normalize();
                }              
                vdir *= 2.0f;              
                this.body.LinearVelocity += vdir;

                //play sound
                if (soundFx) dashSound.Play();

                StartCoolingDown();
                StartDashing();
                return true;
            }
            return false;
        }

        private void StartDashing()
        {
            this.IsDashing = true;
            DashCounter = 0;
        }

        private void StopDashing()
        {
            this.IsDashing = false;
        }

        private void StartCoolingDown()
        {
            
            this.IsCoolingDown = true;
            CoolDownCounter = 0;
        }

        private void StopCoolingDown()
        {
            this.sprite.filtcolor = Color.White;
            this.IsCoolingDown = false;
        }

        public void UpdateDash(GameTime gameTime)
        {
            if (this.IsCoolingDown)
            {
                if (CoolDownCounter <= CoolDownCounterMax)
                { //still cooling down
                    int c = (int)(((float)CoolDownCounter / CoolDownCounterMax) * 255f); //color from 0 to 255
                    this.sprite.filtcolor = new Color(c, c, 255 - c);
                    CoolDownCounter += gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                { //done cooling down
                    this.StopCoolingDown();
                }

            }

            if (this.IsDashing)
            {
                if (DashCounter <= DashCounterMax)
                { //still dashing
                    DashCounter += gameTime.ElapsedGameTime.Milliseconds;
                }
                else
                { //done dashing
                    this.StopDashing();
                }

            }

        }
      
    }
}
