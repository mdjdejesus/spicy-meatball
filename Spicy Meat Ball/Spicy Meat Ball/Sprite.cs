﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Common;
using FarseerPhysics.Common.PolygonManipulation;
using FarseerPhysics.Common.Decomposition;

namespace Spicy_Meat_Ball
{
       

    public class Sprite
    {
        public Texture2D Image;
        protected Vector2 position;
        public Vector2 Origin;
        private float width, height;
        public float scale, rotate;
        public Color filtcolor = new Color(255,255,255);
        public SpriteEffects spriteEffect;

        public float Height
        {
            get { return height; }
        }

        public float Width
        {
            get { return width; }
        }

        public virtual Vector2 Position
        {
            get { return position; }
            set
            {
                position = value;
                Origin.X = Position.X + ( Width / 2);
                Origin.Y = Position.Y + ( Height / 2);
            }
        }
        
         public Sprite()
         {
             Position = new Vector2(0, 0);
         }

         public Sprite( float x, float y)
         {
             Position = new Vector2(x, y);
         }

         public virtual void SetImage(ContentManager contentmanager, string Name, float scale = 1, float rotate = 0, SpriteEffects spriteEffect = SpriteEffects.None)
         {
            Image =  contentmanager.Load<Texture2D>(Name);
            this.scale = scale;
            this.rotate = rotate;
            this.spriteEffect = spriteEffect;
            
            
            if (Math.Abs(rotate) % (float)Math.PI < 0.1f)
            {
                width = scale * Image.Width;
                height = scale * Image.Height;
            }
            else
            {
                width = scale * Image.Height;
                height = scale * Image.Width;
            }
            
         }

         public virtual void Draw(SpriteBatch spritebatch)
         {
             spritebatch.Draw(Image, Position, null, filtcolor, rotate, Vector2.Zero, this.scale, spriteEffect, 0f);
         }
    }

  
    ///////////////////////////////////////////////////////////////////
    public class StaticPolySprite
    {
        private Sprite sprite;
        private Vector2 spriteOrigin;
        private Body body;

        private uint[] polydata;
        private Vertices polyverts;
        List<Vertices> vertlist;

        public StaticPolySprite()
        {
            this.sprite = new Sprite();
        }

        public void SetImage(ContentManager contentmanager, string Name, float scale = 1, float rotate = 0, SpriteEffects spriteEffect = SpriteEffects.None)
        {
            this.sprite.SetImage(contentmanager, Name, scale, rotate, spriteEffect);
            this.spriteOrigin = new Vector2(this.Width / 2, this.Height / 2);
        }

        public void SetBody(World world, Vector2 vPosition)
        {
            //get polygon data
            polydata = new uint[(int)this.sprite.Width * (int)this.sprite.Height];
            this.sprite.Image.GetData(polydata);

            //for flipped images
            if (this.sprite.spriteEffect == SpriteEffects.FlipHorizontally)
            {
                int x;
                for (int y = 0; y < (int)this.sprite.Height; y++)
                {
                    x = y * (int)this.sprite.Width;
                    Array.Reverse(polydata, x, (int)this.sprite.Width);
                }
            }

            polyverts = PolygonTools.CreatePolygon(polydata, (int)this.sprite.Width, false);

            //Vector2 centroid = new Vector2(this.sprite.Width / 2, this.sprite.Width / 2); //-polyverts.GetCentroid();
            //Vector2 centroid = new Vector2(0, 0); // -polyverts.GetCentroid(); //-ConvertUnits.ToSimUnits(new Vector2(this.sprite.Width / 2, this.sprite.Width / 2));
            //polyverts.Translate(ref centroid);

            //We simplify the vertices found in the texture.
            polyverts = SimplifyTools.ReduceByDistance(polyverts, 4f);

            //Since it is a concave polygon, we need to partition it into several smaller convex polygons
            vertlist = Triangulate.ConvexPartition(polyverts, TriangulationAlgorithm.Bayazit);

            Vector2 vertScale = new Vector2(ConvertUnits.ToSimUnits(1));
            foreach (Vertices vertices in vertlist)
            {
                vertices.Scale(ref vertScale);
            }

            //create body
            this.body = BodyFactory.CreateCompoundPolygon(world, vertlist, 1f,  BodyType.Static);
            this.body.IsStatic = true;
            this.body.Restitution = 0.0f;
            this.body.Friction = 0.0f;

            //this.Position = vPosition - ConvertUnits.ToSimUnits(new Vector2(this.sprite.Width / 2, this.sprite.Width / 2));
            this.body.Position = ConvertUnits.ToSimUnits(vPosition);
        }


        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(this.sprite.Image, this.Position + this.spriteOrigin, null, this.sprite.filtcolor, this.sprite.rotate, this.spriteOrigin, this.sprite.scale, this.sprite.spriteEffect, 0f);             
        }

        public float Height
        {
            get { return this.sprite.Height; }
        }

        public float Width
        {
            get { return this.sprite.Width; }
        }

        public virtual Vector2 Position
        {
            get { return ConvertUnits.ToDisplayUnits(this.body.Position); }
            set
            {
                this.body.Position = ConvertUnits.ToSimUnits(value) + ConvertUnits.ToSimUnits(new Vector2(-this.sprite.Width / 2, this.sprite.Height / 2));
            }
        }
        

    }
  //////////////////////////////////////////
 
  
    public class StaticRectSprite
    {
        private Sprite sprite;
        private Body body;

        public StaticRectSprite()
        {
            this.sprite = new Sprite(); 
        }

        public void SetImage(ContentManager contentmanager, string Name, float scale = 1, float rotate = 0, SpriteEffects spriteEffect = SpriteEffects.None)
        {
            this.sprite.SetImage(contentmanager,Name, scale, rotate, spriteEffect);
        }

        public void SetBody(World world, Vector2 vPosition)
        {
            this.body = BodyFactory.CreateRectangle(world, ConvertUnits.ToSimUnits(this.sprite.Width), ConvertUnits.ToSimUnits(this.sprite.Height), 1f);
            this.body.IsStatic = true;
            this.body.Restitution = 0.0f;
            this.body.Friction = 0.0f;

            this.Position = vPosition;
        }

        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(this.sprite.Image, ConvertUnits.ToDisplayUnits(this.body.Position), null, this.sprite.filtcolor, this.sprite.rotate, new Vector2(this.sprite.Width / 2, this.sprite.Height / 2), this.sprite.scale, this.sprite.spriteEffect, 0f);
        }

        public float Height
        {
            get { return this.sprite.Height; }
        }

        public float Width
        {
            get { return this.sprite.Width; }
        }

        public virtual Vector2 Position
        {
            get { return this.sprite.Position; }
            set
            {
                this.sprite.Position = value;
                this.body.Position = ConvertUnits.ToSimUnits(value);
            }
        }

    }

    public class DynamicCircSprite
    {
        protected Sprite sprite;
        protected Body body;
        
        
        public DynamicCircSprite()
        {
            this.sprite = new Sprite();
        }

        public virtual void SetImage(ContentManager contentmanager, string Name, float scale = 1, float rotate = 0, SpriteEffects spriteEffect = SpriteEffects.None)
        {
            this.sprite.SetImage(contentmanager, Name, scale, rotate, spriteEffect);           
        }

        public virtual void SetBody(World world, Vector2 vPosition, float fMass = 1.0f, float fFriction = 0.0f, float fRestitution = 1.0f)
        {
            this.body = BodyFactory.CreateCircle(world, ConvertUnits.ToSimUnits(this.sprite.Width / 2.0f), 1.0f);
            this.body.BodyType = BodyType.Dynamic;
            this.Position = vPosition;
            this.body.Mass = fMass;
            this.body.Friction = fFriction;
            this.body.Restitution = fRestitution;
        }

        public virtual bool MyOnCollision(Fixture f1, Fixture f2, Contact contact)
        {
            //We want the collision to happen, so we return true.
            return true;
        } 


        public virtual void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(this.sprite.Image, ConvertUnits.ToDisplayUnits(this.body.Position), null, this.sprite.filtcolor, this.body.Rotation, new Vector2(this.sprite.Width / 2, this.sprite.Height / 2), this.sprite.scale, this.sprite.spriteEffect, 0f);
        }

        public float Height
        {
            get { return this.sprite.Height; }
        }

        public float Width
        {
            get { return this.sprite.Width; }
        }

        public virtual Vector2 Position
        {
            get { return ConvertUnits.ToDisplayUnits(this.body.Position); }
            set
            {
                this.sprite.Position = value;
                this.body.Position = ConvertUnits.ToSimUnits(value);
            }
        }

        public virtual void ApplyLinearImpulse(Vector2 vDir)
        {
            this.body.ApplyLinearImpulse(ConvertUnits.ToSimUnits(vDir));
        }

        public void ApplyForce(Vector2 vDir)
        {
            this.body.ApplyForce(ConvertUnits.ToSimUnits(vDir));
        }

        public virtual void Stop()
        {
            this.body.ResetDynamics();
        }

        public Vector2 Velocity
        {
            get { return this.body.LinearVelocity; }
        }

    }
       

}
