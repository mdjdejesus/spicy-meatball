using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics;
using FarseerPhysics.DebugView;

namespace Spicy_Meat_Ball
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private World world;

        //game states
        enum GameState
        {
            MainMenu,
            OptionsMenu,
            PauseMenu,
            Playing
        }

        //rectangle sides
        enum Sides
        {
            None,
            Top,
            Bottom,
            Left,
            Right
        }

        GameState currentGameState = GameState.MainMenu;

        //game options
        struct Options
        {
            public Boolean fullScreen;
            public Boolean music;
            public Boolean soundFx;
            public int speed;
            public Boolean debug;
        }

        Options currentOptions = new Options();

        Boolean IsRestartable;

        //menu buttons
        MenuButton btnContinueFromMain, btnPlay, btnOptions, btnQuit;
        MenuButton btnContinue, btnMainMenu;
        MenuButton btnFullScreen, btnMusic, btnSoundFx, btnDebug, btnSpeed;

        //menu positions
        List<Vector2> menuItemPosition = new List<Vector2>();

        //current Menu Item position
        int currentItemPosition;

        // Create an instance of Texture2D that will
        // contain the background textures.
        Texture2D background, menubackground, farseerlogo;

        Texture2D meatballpointer, smallmeatball;
        List<Texture2D> speed = new List<Texture2D>();

        // Create a Rectangle that will define
        // the limits for the main game screen.
        Rectangle mainFrame;
        int gameWidth, gameHeight;
        Body edgeTop, edgeBottom, edgeRight, edgeLeft;
        
        //For the meatball, player, goal post, and goal sprites
        Ball meatball; 

        List<Player> player; // = new List<Player> { new Player(), new Player() };
        List<Goal> goal = new List<Goal> { new Goal(), new Goal() };
        List<StaticPolySprite> goalpost = new List<StaticPolySprite> { new StaticPolySprite(), new StaticPolySprite(), 
            new  StaticPolySprite(), new  StaticPolySprite() };

        Vector2 mb_home, p1_home, p2_home;

        //Fonts
        SpriteFont font,bigfont;

        //Sound Effects and Songs
        SoundEffect gulpSound, applauseSound;
        Song gameSong;

        int winner = 0; //0 = no winner, else 1 or 2 for the player number
        int numMeatBallsToWin = 5;
        Texture2D greenwins, redwins;

        Boolean IsApplausePlaying = false;

        //For Pausing
        int MaxKeyDelay = 1000; //two seconds
        int KeyDelayCounter = 0;

        //For DPad
        int MaxDPadDelay = 100; // 1/2 second
        int DPadDelayCounter = 0;

        //For debug view
        protected DebugViewXNA DebugView;
        Matrix _Projection;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            //gameWidth = 1024; gameHeight = 768;
            gameWidth = (int)(0.9f * (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width);
            gameHeight = (int)(0.9f * (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height);

            graphics.PreferredBackBufferWidth = gameWidth;
            graphics.PreferredBackBufferHeight = gameHeight;

            graphics.IsFullScreen = false;

            mainFrame = new Rectangle(0, 0, gameWidth, gameHeight);

            Content.RootDirectory = "Content";

            //Create a world with no gravity.
            world = new World(new Vector2(0, 0)); 

        }

        public void ResetGame ()
        {
            if (MediaPlayer.State == MediaState.Stopped)
            { if (currentOptions.music) MediaPlayer.Play(gameSong); }

            player[0].Position = p1_home;
            player[1].Position = p2_home;

            goal[0].NumberMeatballs = goal[1].NumberMeatballs = 0;

            ResetRound();

            winner = 0;
            IsApplausePlaying = false;
        }

        public void ResetRound()
        {
            if(currentOptions.music) MediaPlayer.Play(gameSong);

            meatball.Stop();
            player[0].Stop();
            player[1].Stop();

            meatball.Position = mb_home;

        }

        private void ChangeScreenMode(bool isFull)
        {
            if (isFull)
            {
                this.graphics.ToggleFullScreen();

                gameWidth = (int)(1f * (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width);
                gameHeight = (int)(1f * (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height);

                graphics.PreferredBackBufferWidth = gameWidth;
                graphics.PreferredBackBufferHeight = gameHeight;

                graphics.ApplyChanges();
            }
            else
            {
                this.graphics.ToggleFullScreen();
                this.graphics.IsFullScreen = false;

                gameWidth  = (int)(0.9f * (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width);
                gameHeight = (int)(0.9f * (float)GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height);

                graphics.PreferredBackBufferWidth = gameWidth;
                graphics.PreferredBackBufferHeight = gameHeight;

                graphics.ApplyChanges();
            }

            mainFrame = new Rectangle(0, 0, gameWidth, gameHeight);
        }

        protected override void Initialize()
        {
            
            currentOptions.fullScreen = false;
            currentOptions.music = true;
            currentOptions.soundFx = true;
            currentOptions.speed = 4;
            currentOptions.debug = false;

            graphics.IsFullScreen = currentOptions.fullScreen;

            IsRestartable = false;

            currentItemPosition = 0;
            menuItemPosition.Add(new Vector2(50, 2 * gameHeight / 3));
            Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y);

            base.Initialize();
        }

       
        protected override void LoadContent()
        {
            DebugView = new DebugViewXNA(world);
           
            DebugView.DefaultShapeColor = Color.White;
            DebugView.SleepingShapeColor = Color.White;
            DebugView.AppendFlags(DebugViewFlags.Shape);
            DebugView.AppendFlags(DebugViewFlags.PolygonPoints);
            DebugView.AppendFlags(DebugViewFlags.DebugPanel);
            DebugView.AppendFlags(DebugViewFlags.PerformanceGraph);
            DebugView.AppendFlags(DebugViewFlags.Shape);
            DebugView.AppendFlags(DebugViewFlags.ContactNormals);
            DebugView.AppendFlags(DebugViewFlags.ContactPoints);


            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Farseer expects objects to be scaled to MKS (meters, kilos, seconds)
            // 1 meters equals 48 pixels here
            ConvertUnits.SetDisplayUnitToSimUnitRatio(48f);

            // Load Font
            font = Content.Load<SpriteFont>("gameFont");
            bigfont = Content.Load<SpriteFont>("gameBigFont");

            // Load the background content.
            background = Content.Load<Texture2D>("bluetablecloth_1024x768");

            // Load the menu background content.
            menubackground = Content.Load<Texture2D>("menubackground");

            // Load Farseer logo image
            farseerlogo = Content.Load<Texture2D>("FarseerPhysicsLogo");

            // Set the rectangle parameters.
            mainFrame = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
           
            //Load Sound Effects an Songs
            gulpSound = Content.Load<SoundEffect>("gulp");
            applauseSound = Content.Load<SoundEffect>("applause");
            gameSong = Content.Load<Song>("maintheme");

            MediaPlayer.IsRepeating = true;

            // Place meatball in center
            meatball = new Ball(this.Content);
            meatball.SetImage(this.Content, "meatball");
            meatball.SetBody(world, new Vector2 (gameWidth / 2, gameHeight / 2), 0.5f);
 
            player = new List<Player> { new Player(this.Content), new Player(this.Content) };

            //place red player in left center
            player[0].SetPlayerImages(this.Content, "redplayer","fire-trail");
            player[0].SetBody(world, new Vector2(gameWidth / 4, gameHeight / 2), 1.0f);

            //place green player in right center
            player[1].SetPlayerImages(this.Content, "greenplayer", "fire-trail");
            player[1].SetBody(world, new Vector2(gameWidth - gameWidth / 4, gameHeight / 2), 1.0f);

            //place red goal in left center
            goal[0].SetImage(this.Content, "redgoal");

            //place green goal in right center
            goal[1].SetImage(this.Content, "greengoal");

            //Place goal posts
            goalpost[0].SetImage(this.Content, "fork",1f, 0.0f); 
            goalpost[0].SetBody(world, new Vector2(0, 0));
            
            goalpost[1].SetImage(this.Content, "spoon", 1f, 0.0f);
            goalpost[1].SetBody(world, new Vector2(0, 0));
           
            goalpost[2].SetImage(this.Content, "spoon", 1f, 0.0f, SpriteEffects.FlipHorizontally);
            goalpost[2].SetBody(world, new Vector2(0, 0));

            goalpost[3].SetImage(this.Content, "fork", 1f, 0.0f, SpriteEffects.FlipHorizontally);
            goalpost[3].SetBody(world, new Vector2(0, 0));

            //menu button stuff
            // ---Main Menu--
            IsMouseVisible = false;

            /////////menu item images
            // ---Options Menu--
            smallmeatball = Content.Load<Texture2D>("smallmeatball");
            meatballpointer = Content.Load<Texture2D>("meatballpointer");

            //game win banners
            redwins = Content.Load<Texture2D>("redwinner");
            greenwins = Content.Load<Texture2D>("greenwinner");

            speed.Add(Content.Load<Texture2D>("fire-ball-low"));
            speed.Add(Content.Load<Texture2D>("fire-ball-medium"));
            speed.Add(Content.Load<Texture2D>("fire-ball-mediumhigh"));
            speed.Add(Content.Load<Texture2D>("fire-ball-high"));
            btnPlay = new MenuButton(Content.Load<Texture2D>("startbutton"), graphics.GraphicsDevice);
            btnContinueFromMain = new MenuButton(Content.Load<Texture2D>("continuebutton"), graphics.GraphicsDevice);
            btnPlay = new MenuButton(Content.Load<Texture2D>("startbutton"), graphics.GraphicsDevice);
            btnOptions = new MenuButton(Content.Load<Texture2D>("optionsbutton"), graphics.GraphicsDevice);
            btnQuit = new MenuButton(Content.Load<Texture2D>("quitbutton"), graphics.GraphicsDevice);
            btnMainMenu = new MenuButton(Content.Load<Texture2D>("mainmenubutton"), graphics.GraphicsDevice);
            btnContinue = new MenuButton(Content.Load<Texture2D>("continuebutton"), graphics.GraphicsDevice);
            btnFullScreen = new MenuButton(Content.Load<Texture2D>("optfullscreenbutton"), graphics.GraphicsDevice);
            btnMusic = new MenuButton(Content.Load<Texture2D>("optmusicbutton"), graphics.GraphicsDevice);
            btnSoundFx = new MenuButton(Content.Load<Texture2D>("optsoundfxbutton"), graphics.GraphicsDevice);
            btnSpeed = new MenuButton(Content.Load<Texture2D>("optmeatballspeedbutton"), graphics.GraphicsDevice);
            btnDebug = new MenuButton(Content.Load<Texture2D>("optdebugbutton"), graphics.GraphicsDevice);

            setPositions();
            
            if (currentOptions.music) MediaPlayer.Play(gameSong);

            DebugView.LoadContent(GraphicsDevice, this.Content);

            //_Projection = Matrix.CreateOrthographicOffCenter(0f, ConvertUnits.ToSimUnits(GraphicsDevice.Viewport.Width), ConvertUnits.ToSimUnits(GraphicsDevice.Viewport.Height), 0f, 0f, 1f); 
            
               
        }

        private void setPositions()
        {
            // Place meatball in center
            meatball.Position = new Vector2(gameWidth / 2 , gameHeight / 2);
            mb_home =  meatball.Position;

            //place red goal in left center
            goal[0].Position = new Vector2(0, (gameHeight / 2) - (goal[0].Height / 2));

            //place green goal in right center
            goal[1].Position = new Vector2(gameWidth - goal[1].Width, (gameHeight / 2) - (goal[1].Height / 2));

            //create Edge of Screen for collisions
            if (edgeTop != null) edgeTop.Dispose();
            edgeTop = BodyFactory.CreateEdge(world, //TOP
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Left, mainFrame.Top)),
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Right, mainFrame.Top)));
            if (edgeBottom != null) edgeBottom.Dispose();
            edgeBottom = BodyFactory.CreateEdge(world, //BOTTOM
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Left, mainFrame.Bottom)),
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Right, mainFrame.Bottom)));
            if (edgeLeft != null) edgeLeft.Dispose();
            edgeLeft = BodyFactory.CreateEdge(world, //LEFT
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Left, mainFrame.Top)),
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Left, mainFrame.Bottom)));
            if (edgeRight != null) edgeRight.Dispose();
            edgeRight = BodyFactory.CreateEdge(world, //RIGHT
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Right, mainFrame.Top)),
                ConvertUnits.ToSimUnits(new Vector2(mainFrame.Right, mainFrame.Bottom)));

            edgeTop.Restitution = edgeBottom.Restitution = edgeLeft.Restitution = edgeRight.Restitution = 0.0f;
            edgeTop.Friction = edgeBottom.Friction = edgeLeft.Friction = edgeRight.Friction = 0.0f;

            //set goal posts
            goalpost[0].Position = new Vector2(goalpost[0].Width / 2, goal[0].Origin.Y - goalpost[0].Height - goal[0].Width);
            goalpost[1].Position = new Vector2(goalpost[1].Width / 2, goal[0].Origin.Y - goalpost[1].Height + goal[0].Width);
            goalpost[2].Position = new Vector2(gameWidth - goalpost[2].Width / 2, goal[0].Origin.Y - goalpost[0].Height - goal[0].Width);
            goalpost[3].Position = new Vector2(gameWidth - goalpost[3].Width / 2, goal[0].Origin.Y - goalpost[1].Height + goal[0].Width);

            //place red player in left center
            player[0].Position = new Vector2(gameWidth / 4, gameHeight / 2);
            p1_home = player[0].Position;

            //place green player in right center
            player[1].Position = new Vector2(gameWidth - gameWidth / 4, gameHeight / 2);
            p2_home =  player[1].Position;

            ///////// menu button stuff /////

            //menu item positions
            if (menuItemPosition != null) menuItemPosition.Clear();
            menuItemPosition.Add(new Vector2(50, 2 * gameHeight / 3));
            menuItemPosition.Add(new Vector2(gameWidth / 2 - btnPlay.size.X / 2, 2 * gameHeight / 3));
            menuItemPosition.Add(new Vector2(gameWidth / 2 - btnPlay.size.X / 2, (2 * gameHeight / 3) + 60));
            menuItemPosition.Add(new Vector2(gameWidth / 2 - btnPlay.size.X / 2, (2 * gameHeight / 3) + 120));
            menuItemPosition.Add(new Vector2(gameWidth / 2 - btnPlay.size.X / 2, (2 * gameHeight / 3) + 180));

            // ---Main Menu--
            btnContinueFromMain.setPosition(menuItemPosition[0]);

            btnPlay.setPosition(menuItemPosition[1]);

            btnOptions.setPosition(menuItemPosition[2]);

            btnQuit.setPosition(menuItemPosition[3]);

            // ---Pause Menu--
            btnMainMenu.setPosition(menuItemPosition[0]);

            btnContinue.setPosition(menuItemPosition[1]);


            // ---Options Menu--
            btnFullScreen.setPosition(menuItemPosition[1]);

            btnMusic.setPosition(menuItemPosition[2]);

            btnSoundFx.setPosition(menuItemPosition[3]);

            btnSpeed.setPosition(menuItemPosition[4]);

            btnDebug.setPosition(new Vector2(smallmeatball.Width + 30, (2 * gameHeight / 3) + 180));

            _Projection = Matrix.CreateOrthographicOffCenter(0f, ConvertUnits.ToSimUnits(GraphicsDevice.Viewport.Width), ConvertUnits.ToSimUnits(GraphicsDevice.Viewport.Height), 0f, 0f, 1f);
               
        }

        
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        private void MainMenuUpdate(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();

            if (IsRestartable)
            {
                if (btnContinueFromMain.isClicked == true) { currentGameState = GameState.Playing; currentItemPosition = 0; }
                btnContinueFromMain.Update(mouse);
            }

            if (btnPlay.isClicked == true) { currentGameState = GameState.Playing; ResetGame(); currentItemPosition = 0; }
            btnPlay.Update(mouse);

            if (btnOptions.isClicked == true) { currentGameState = GameState.OptionsMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0; }
            btnOptions.Update(mouse);

            if (btnQuit.isClicked == true) this.Exit();
            btnQuit.Update(mouse);
        }

        private void OptionsMenuUpdate(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();

            if (btnMainMenu.isClicked == true) { currentGameState = GameState.MainMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0; }
            btnMainMenu.Update(mouse);

            if (btnFullScreen.isClicked == true) { 
                currentOptions.fullScreen = !(currentOptions.fullScreen);
   
                //this.graphics.ToggleFullScreen();

                ChangeScreenMode(currentOptions.fullScreen);

                setPositions();              
            }

            btnFullScreen.Update(mouse);

            if (btnMusic.isClicked == true)
            {
                currentOptions.music = !(currentOptions.music);
                if (currentOptions.music) { MediaPlayer.Play(gameSong); } else { MediaPlayer.Stop(); }
            }
            btnMusic.Update(mouse);

            if (btnSoundFx.isClicked == true)
            {
                meatball.soundFx = player[0].soundFx = player[1].soundFx = currentOptions.soundFx = !(currentOptions.soundFx);
            }
            btnSoundFx.Update(mouse);

            if (btnSpeed.isClicked == true)
            {
                ++currentOptions.speed;
                if (currentOptions.speed > 4) currentOptions.speed = 1;
                meatball.Speed = player[0].Speed = player[1].Speed = currentOptions.speed;
            }
            btnSpeed.Update(mouse);

            if (btnDebug.isClicked == true) currentOptions.debug = !(currentOptions.debug);
            btnDebug.Update(mouse);
        }

        private void PauseMenuUpdate(GameTime gameTime)
        {
            MouseState mouse = Mouse.GetState();

            if (btnMainMenu.isClicked == true) { currentGameState = GameState.MainMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0; }
            btnMainMenu.Update(mouse);

            if (btnContinue.isClicked == true) { currentGameState = GameState.Playing; currentItemPosition = 0; }
            btnContinue.Update(mouse);

            if (btnOptions.isClicked == true) { currentGameState = GameState.OptionsMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0; }
            btnOptions.Update(mouse);
            
            if (btnQuit.isClicked == true) this.Exit();
            btnQuit.Update(mouse);
        }

        private void PlayingUpdate(GameTime gameTime)
        {
            SpecialAction sa;
 
            if (winner != 0)
            {
                // Allows the game to reset
                if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed
                    || Keyboard.GetState().IsKeyDown(Keys.Space))
                {
                    //Reset Game
                    ResetGame();
                    IsRestartable = false;
                }
            }

            foreach  (PlayerIndex pi in new PlayerIndex[] {PlayerIndex.One, PlayerIndex.Two})
            {
                player[(int)pi].ApplyLinearImpulse(HandleMovementInput(pi));
                
                sa = HandleSpecialInput(pi);

                if (sa == SpecialAction.Dash) { player[(int)pi].StartDash(); }
                if (sa == SpecialAction.Stop) { player[(int)pi].Stop(); }

                player[(int)pi].UpdateDash(gameTime);
            }

            if (winner == 0) { MadeGoal(); }

            world.Step((float)gameTime.ElapsedGameTime.TotalSeconds);
        }

        
        //special player actions
        enum SpecialAction
        {
            Nothing,
            Dash,
            Stop
        }

        private SpecialAction HandleSpecialInput(PlayerIndex playerindex)
        {
            SpecialAction sa = SpecialAction.Nothing;

            Keys[,] keymap = new Keys[,] {
                            {Keys.LeftControl, Keys.LeftAlt},
                            {Keys.RightControl, Keys.RightAlt}
                            };

            if (GamePad.GetState(playerindex).IsConnected)
            {
                if (GamePad.GetState(playerindex).Buttons.A == ButtonState.Pressed)
                {
                    sa = SpecialAction.Dash;
                }
                if (GamePad.GetState(playerindex).Buttons.B == ButtonState.Pressed)
                {
                    sa = SpecialAction.Stop;
                }

            }
            else
            {
                if (Keyboard.GetState().IsKeyDown(keymap[(int)playerindex, 0]))
                {
                    sa = SpecialAction.Dash; 
                }
                if (Keyboard.GetState().IsKeyDown(keymap[(int)playerindex, 1]))
                {
                    sa = SpecialAction.Stop;
                }
            }

            return sa;
        }


        private Vector2 HandleMovementInput(PlayerIndex playerindex)
        {
            Vector2 vDir = Vector2.Zero;

            Keys[,] keymap = new Keys[,] {
                            {Keys.LeftShift, Keys.W, Keys.S, Keys.A, Keys.D},
                            {Keys.RightShift, Keys.Up, Keys.Down, Keys.Left, Keys.Right}
                            };

            if (GamePad.GetState(playerindex).IsConnected)
            {
                
                if ((Math.Abs(GamePad.GetState(playerindex).ThumbSticks.Left.X)
                    + Math.Abs(GamePad.GetState(playerindex).ThumbSticks.Left.Y)) > 0.0f)
                {

                    vDir = GamePad.GetState(playerindex).ThumbSticks.Left;
                    vDir.Y = -vDir.Y;
                }
                
            }
            else
            {
                if (Keyboard.GetState().IsKeyDown(keymap[ (int)playerindex, 0]))
                {
                    //if (player[0].StartDash()) { if (currentOptions.soundFx) dashSound.Play(); } 
                }


                if (Keyboard.GetState().IsKeyDown(keymap[(int)playerindex, 1]))
                {
                    vDir.Y -= 1;
                }
                if (Keyboard.GetState().IsKeyDown(keymap[(int)playerindex, 2]))
                {
                    vDir.Y += 1;
                }
                if (Keyboard.GetState().IsKeyDown(keymap[(int)playerindex, 3]))
                {
                    vDir.X -= 1;
                }
                if (Keyboard.GetState().IsKeyDown(keymap[(int)playerindex, 4]))
                {
                    vDir.X += 1;
                }
                
            }

            return vDir;
        }

        protected void UpdateMousePointer(GameTime gameTime)
        {
            float gt = gameTime.ElapsedGameTime.Milliseconds;
            gt *= 0.25f; //slow down the cursor

            Boolean DPadPressed = false;

            if (GamePad.GetState(PlayerIndex.One).IsConnected)
            {
                Mouse.SetPosition( Mouse.GetState().X + (int)(gt * GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X),
                                   Mouse.GetState().Y - (int)(gt * GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y));

                if (DPadDelayCounter <= 0)
                {
                    if (GamePad.GetState(PlayerIndex.One).DPad.Down == ButtonState.Pressed ||
                        GamePad.GetState(PlayerIndex.One).DPad.Right == ButtonState.Pressed)
                    {
                        DPadPressed = true;
                        currentItemPosition++;
                        if (currentItemPosition >= menuItemPosition.Count) currentItemPosition = 0;
                    }

                    if (GamePad.GetState(PlayerIndex.One).DPad.Up == ButtonState.Pressed ||
                        GamePad.GetState(PlayerIndex.One).DPad.Left == ButtonState.Pressed)
                    {
                        DPadPressed = true;
                        currentItemPosition--;
                        if (currentItemPosition <= 0) currentItemPosition = menuItemPosition.Count -1;
                    }

                    if (DPadPressed) Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y);
                    DPadDelayCounter = MaxDPadDelay;
                }
                
            }
        }


        protected override void Update(GameTime gameTime)
        {

            // Allows the game to exit with escape key
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                this.Exit();
            }

            //allow the game to be paused/continued using space bar
            if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed ||
                GamePad.GetState(PlayerIndex.Two).Buttons.Start == ButtonState.Pressed ||
                        Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (winner == 0)
                {
                    if (KeyDelayCounter <= 0)
                    {
                        if (currentGameState == GameState.Playing) { currentGameState = GameState.PauseMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0; }
                        else if (currentGameState == GameState.PauseMenu) { currentGameState = GameState.Playing; currentItemPosition = 0; }
                        KeyDelayCounter = MaxKeyDelay;
                    }
                }
                else
                {
                    if (KeyDelayCounter <= 0)
                    {
                        currentGameState = GameState.Playing; currentItemPosition = 0;
                        KeyDelayCounter = MaxKeyDelay;
                        ResetGame();
                    }
                }
            }
            else { KeyDelayCounter -= gameTime.ElapsedGameTime.Milliseconds; DPadDelayCounter -= gameTime.ElapsedGameTime.Milliseconds; }

            switch (currentGameState)
            {
                case GameState.MainMenu:
                    // Allows the game to exit using back button
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
                    {
                        currentGameState = GameState.Playing; currentItemPosition = 0;
                    }
                    else { MainMenuUpdate(gameTime); UpdateMousePointer(gameTime); }
                    
                    break;
                case GameState.OptionsMenu:
                    // Allows the game to exit using back button
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Start == ButtonState.Pressed)
                    {
                        currentGameState = GameState.MainMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0;
                    }
                    else { OptionsMenuUpdate(gameTime); UpdateMousePointer(gameTime); }

                    break;
                case GameState.PauseMenu:
                    // Allows the game to exit using back button
                    if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                    {
                        currentGameState = GameState.MainMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0;
                    }
                    else { PauseMenuUpdate(gameTime); UpdateMousePointer(gameTime); }

                    break;
                case GameState.Playing:
                    IsRestartable = true;
                    // Allows the game to go back to Main Menu using back button
                       if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                    {
                        currentGameState = GameState.MainMenu; Mouse.SetPosition((int)menuItemPosition[currentItemPosition].X, (int)menuItemPosition[currentItemPosition].Y); currentItemPosition = 0;
                    }
                    else { PlayingUpdate(gameTime); }
                    
                    break;
            }

            base.Update(gameTime);
        }

       
        private void MadeGoal()
        {
            float minDist;
            float dist; 
            
            for (int gi = 0; gi < goal.Count; gi++)
            {
                minDist = goal[gi].Width / 2 - meatball.Width / 2;
                dist = Vector2.Distance(goal[gi].Origin, meatball.Position);

                if (dist <= minDist)
               {
                   if (currentOptions.soundFx) gulpSound.Play();
                    ++goal[gi].NumberMeatballs;
                    if (goal[gi].NumberMeatballs >= numMeatBallsToWin) { winner = gi + 1; }
                    ResetRound();
                }
            }
         
        }
      
       
        private void PlayingDraw (GameTime gameTime)
        {

            spriteBatch.Draw(background, mainFrame, Color.White);

            
            //if (currentOptions.debug)
            //{
            //    spriteBatch.Draw(background, new Rectangle(0, 0, gameWidth, 80),
            //        new Rectangle(0, 0, gameWidth, 80),
            //        Color.Blue);
            //    int ofs = gameWidth / 12;
            //    spriteBatch.DrawString(font, "POS: " + player[0].Position.ToString(), new Vector2(ofs, 10), Color.Red);
            //    spriteBatch.DrawString(font, "VEL: " + player[0].Velocity.ToString(), new Vector2(ofs, 30), Color.Red);
            //    spriteBatch.DrawString(font, "SPD: " + player[0].Speed.ToString(), new Vector2(ofs, 50), Color.Red);
            //    spriteBatch.DrawString(font, "POS: " + meatball.Position.ToString(), new Vector2(ofs + gameWidth / 3, 10), Color.SaddleBrown);
            //    spriteBatch.DrawString(font, "VEL: " + meatball.Velocity.ToString(), new Vector2(ofs + gameWidth / 3, 30), Color.SaddleBrown);
            //    spriteBatch.DrawString(font, "SPD: " + meatball.Speed.ToString(), new Vector2(ofs + gameWidth / 3, 50), Color.SaddleBrown);
            //    spriteBatch.DrawString(font, "POS: " + player[1].Position.ToString(), new Vector2(ofs + 2 * gameWidth / 3, 10), Color.Green);
            //    spriteBatch.DrawString(font, "VEL: " + player[1].Velocity.ToString(), new Vector2(ofs + 2 * gameWidth / 3, 30), Color.Green);
            //    spriteBatch.DrawString(font, "SPD: " + player[1].Speed.ToString(), new Vector2(ofs + 2 * gameWidth / 3, 50), Color.Green);
            //}

            goal[0].Draw(spriteBatch);
            goal[1].Draw(spriteBatch);
            goalpost[0].Draw(spriteBatch);
            goalpost[1].Draw(spriteBatch);
            goalpost[2].Draw(spriteBatch);
            goalpost[3].Draw(spriteBatch);

            meatball.Draw(spriteBatch);

            player[0].Draw(spriteBatch);
            player[1].Draw(spriteBatch);

            if (winner != 0)
            {
                if (winner == 1)
                {
                    spriteBatch.Draw(redwins, new Vector2((gameWidth / 2) - (redwins.Width / 2), 10), Color.White);
                }
                else if (winner == 2)
                {
                    spriteBatch.Draw(greenwins, new Vector2((gameWidth / 2) - (greenwins.Width / 2), 10), Color.White);
                }

                if (!IsApplausePlaying) { IsApplausePlaying = true; if (currentOptions.soundFx) applauseSound.Play(); }

                spriteBatch.DrawString(font, "Press <Start> or <Space Bar> To Restart Game.",
                            new Vector2((gameWidth / 2) - (font.MeasureString("Press <Start> or <Space Bar> To Restart Game.").X / 2),
                                         gameHeight - font.MeasureString("Press <Start> or <Space Bar> To Restart Game.").Y - 10),
                            Color.White);

                if (MediaPlayer.State == MediaState.Playing)
                {
                    MediaPlayer.Stop();
                }
            }

            
        }

        private void MainMenuDraw (GameTime gameTime)
        {
            spriteBatch.Draw(menubackground, mainFrame, Color.White);
            spriteBatch.DrawString(font, "Developed by Javier DeJesus (2013)", new Vector2(10, 10), Color.White);
            if (IsRestartable) { btnContinueFromMain.Draw(spriteBatch); }
            btnPlay.Draw(spriteBatch);
            btnOptions.Draw(spriteBatch);
            btnQuit.Draw(spriteBatch);

            spriteBatch.Draw(farseerlogo, new Vector2(mainFrame.Width - farseerlogo.Width, mainFrame.Height - farseerlogo.Height), Color.White);
        }

        private void OptionsMenuDraw(GameTime gameTime)
        {
            spriteBatch.Draw(menubackground, mainFrame, Color.White);
            spriteBatch.DrawString(font, "Developed by Javier DeJesus (2013)", new Vector2(10, 10), Color.White);

            spriteBatch.Draw(speed[currentOptions.speed - 1],
                new Vector2(btnSpeed.Position.X - 50, btnSpeed.Position.Y),
                null, Color.White,0,Vector2.Zero,0.25f,SpriteEffects.None ,0); 

            btnMainMenu.Draw(spriteBatch);
            btnFullScreen.Draw(spriteBatch); if (currentOptions.fullScreen) { spriteBatch.Draw(smallmeatball, new Vector2(btnFullScreen.Position.X - 50, btnFullScreen.Position.Y), Color.White);}
            btnMusic.Draw(spriteBatch); if (currentOptions.music) { spriteBatch.Draw(smallmeatball, new Vector2(btnMusic.Position.X - 50, btnMusic.Position.Y), Color.White);}
            btnSoundFx.Draw(spriteBatch); if (currentOptions.soundFx) { spriteBatch.Draw(smallmeatball, new Vector2(btnSoundFx.Position.X - 50, btnSoundFx.Position.Y), Color.White); }
            btnSpeed.Draw(spriteBatch); 

            btnDebug.Draw(spriteBatch); if (currentOptions.debug) { spriteBatch.Draw(smallmeatball, new Vector2(btnDebug.Position.X - 50, btnDebug.Position.Y), Color.White); }
            spriteBatch.Draw(farseerlogo, new Vector2(mainFrame.Width - farseerlogo.Width, mainFrame.Height - farseerlogo.Height), Color.White);
        }

        private void PauseMenuDraw(GameTime gameTime)
        {
            spriteBatch.Draw(menubackground, mainFrame, Color.White);
            spriteBatch.DrawString(font, "Developed by Javier DeJesus (2013)", new Vector2(10, 10), Color.White);

            btnMainMenu.Draw(spriteBatch);
            btnContinue.Draw(spriteBatch);
            btnOptions.Draw(spriteBatch);
            btnQuit.Draw(spriteBatch);

            spriteBatch.Draw(farseerlogo, new Vector2(mainFrame.Width - farseerlogo.Width, mainFrame.Height - farseerlogo.Height), Color.White);
        }

        private void DrawMousePointer(GameTime gameTime)
        {
            spriteBatch.Draw(meatballpointer, new Vector2(Mouse.GetState().X, Mouse.GetState().Y), Color.White);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            switch (currentGameState)
            {
                case GameState.MainMenu:
                    MainMenuDraw(gameTime);
                    spriteBatch.DrawString(bigfont, "- Main Menu -",
                        new Vector2((gameWidth / 2) - (bigfont.MeasureString("- Main Menu -").X / 2), gameHeight / 2 + (bigfont.MeasureString(" ").Y / 2) ),
                        Color.White);
                    DrawMousePointer(gameTime);
                    break;
                case GameState.OptionsMenu:
                    OptionsMenuDraw(gameTime);
                    spriteBatch.DrawString(bigfont, "- Options -",
                        new Vector2((gameWidth / 2) - (bigfont.MeasureString("- Options -").X / 2), gameHeight / 2 + (bigfont.MeasureString(" ").Y / 2) ),
                        Color.White);
                    DrawMousePointer(gameTime);
                    break;
                case GameState.PauseMenu:
                    PauseMenuDraw(gameTime);
                    spriteBatch.DrawString(bigfont, "- Pause -",
                        new Vector2((gameWidth / 2) - (bigfont.MeasureString("- Pause -").X / 2), gameHeight / 2 +(bigfont.MeasureString(" ").Y / 2) ),
                        Color.White);
                    DrawMousePointer(gameTime);
                    break;
                case GameState.Playing:
                    PlayingDraw(gameTime);
                    break;
            }  

            spriteBatch.End();

            if (currentGameState == GameState.Playing && currentOptions.debug)
            {
                DebugView.RenderDebugData(ref _Projection);
            }

            base.Draw(gameTime);
        }
    }
}
